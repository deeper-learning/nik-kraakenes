<?php

use Monolog\Handler\ChromePHPHandler;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

$logger = new Logger('application');

$whoops->pushHandler(
    new PrettyPageHandler()
);

$whoops = new Run();

$whoops->register();

// new logger
$logger->pushHandler(
    new RotatingFileHandler(
        dirname(__DIR__) . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'application.log',
        5,
        Logger::WARNING
    )
);

// dev env chrome plugin
if (isset($_ENV['APP_ENV']) && $_ENV['APP_ENV'] === 'development') {
    $logger->pushHandler(
        new ChromePHPHandler()
    );
}





