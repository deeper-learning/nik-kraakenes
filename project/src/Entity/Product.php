<?php

namespace App\Entity;

class Product
{
    public int $id;
    public string $title;
    public string $description;
    public ?string $imagePath;
    public ?string $category;
    public ?string $crew;
    public ?string $weapons;
    public ?string $endurance;
    public ?float $average_rating;
    /** @var  CheckIn[]*/
    private array $checkIns = [];

    public function __construct()
    {
        //echo 'I was instantiated';
    }

    public function addCheckin(CheckIn $checkIn) : void
    {
        $this->checkIns[] = $checkIn;
    }

    public function getCheckins() : array
    {
        return $this->checkIns;
    }
}


/*private array $checkIns =[];

public function addCheckin(CheckIn $checkIn): void
{
    $this->checkins[] = $checkIn;
}

public function getCheckins(): array
{
    return $this->checkins;
}*/