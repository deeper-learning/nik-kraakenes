<?php

use App\DataProvider\DatabaseProvider;
use Dotenv\Dotenv;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Whoops\Handler\PrettyPageHandler;
use Whoops\Run;

require_once '../vendor/autoload.php';

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

$whoops = new Run();
$whoops->pushHandler(
    new PrettyPageHandler()
);
$whoops->register();

$logger = new Logger('application');
/*$logger->pushHandler(
    new \Monolog\Handler\StreamHandler(
        'application.log' ,
        \Monolog\Logger::WARNING
    )
);*/

// new logger
$logger->pushHandler(
    new RotatingFileHandler(
        dirname(__DIR__) . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'application.log',
         5,
         Logger::WARNING
    )
);

// dev env chrome plugin
if (isset($_ENV['APP_ENV']) && $_ENV['APP_ENV'] === 'development') {
    $logger->pushHandler(
        new \Monolog\Handler\ChromePHPHandler()
    );
}

$dbProvider = new DatabaseProvider();

session_start();

if (isset($_SESSION['loginId'])) {
   $loggedInUser = $dbProvider->getUser($_SESSION['loginId']);
}


/*try {
    $dbh = new PDO(
        'mysql:host=mysql;dbname=project',
        $_ENV['DBUSERNAME'],
        $_ENV['DBPASSWORD']
    );
} catch (PDOException $e) {
    // We could log this!
    die('Unable to establish a database connection');
}*/


