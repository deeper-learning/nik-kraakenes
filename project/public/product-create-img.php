<?php

use App\Entity\Product;

require_once '../src/setup.php';

if (!empty($_POST['title']) && !empty($_POST['description'])) {
    if($_FILES['image_path']['error']) === UPLOAD_ERR_OK) {
    move_uploaded_file($_FILES['image_path']['tmp_name'], 'assets')
    }
    $formData = [
        'title' => strip_tags($_POST['title']),
        'description' => strip_tags($_POST['description']),
        'image_path' => $imageName,
    ];

    $formProduct = new Product();
    $formProduct->title = $formData['title'];
    $formProduct->description = $formData['description'];
    $formProduct->imagePath = $formData['image_path'];

    // Create Product
    $product = $dbProvider->createProduct($formProduct);
    $logger->info('Product created: ' . $product->title);
    header('Location: product.php?productId=' . $product->id);
    exit;
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'template_parts/header_includes.php' ?>
    <!--
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <link href="engine2.css" rel="stylesheet">
    -->
    <title>Create Product</title>

</head>
<body>
<div class="container">
<?php /*include 'template_parts/navbar.php'; */?>
    <div class="card p-4 stickery">
        <h1>Create Product</h1>
        <?php
        if (!empty($error)) {
            switch ($error) {
                case UPLOAD_ERR_INI_SIZE:
                    echo 'The image was too large, please keep it less than' . ini_get('upload_max_filesize');
                    break;
            }
        }
        ?>
        <form method="post" enctype="multipart/form-data">
            <div class"col-md-6 col-sm-6 text-center">
            <label for="image">Image</label>
<input type="file" accept="image/png, image/jpe" name="image_path" id="image" class=" ">
            <small id="fileUpload" class="form-text text-muted">Image types
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <label class="whitely" for="title">Title</label>
                    <input class="form-control" name="title" id="title" placeholder="Title">
                    <label class="whitely" for="description">Description</label>
                    <textarea class="form-control" name="description" id="description" placeholder="Description" rows="10"></textarea>
                </div>
                <div class="col-md-6 col-sm-6">
                    <label class="whitely" for="title">Categories</label>
                    <input class="form-control" name="title" id="title" placeholder="Categories">
                    <label class="whitely" for="title">Crew & Passengers</label>
                    <input class="form-control" name="title" id="title" placeholder="Crew">
                    <label class="whitely" for="title">Weaponry</label>
                    <input class="form-control" name="title" id="title" placeholder="Weapons">
                    <label class="whitely" for="title">Effective Range</label>
                    <input class="form-control" name="title" id="title" placeholder="Range">
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-secondary">Create</button>
        </form>
    </div>
</div>

<?php include 'template_parts/footer_includes.php'; ?>
</body>
</html>
