<?php



?>

<nav class="navbar navbar-expand-sm navbar-dark bg-dark" style="background-color: #5b5c57;">
    <img class="img-fluid " src="assets/logo.png"  alt="Rating Rockets Logo">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto align-items-sm-end">
            <li class="nav-item active">
                <a class="nav-link" href="home.php">Home <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="product-search.php">Search</a>
            </li>
            <?php if (!empty($loggedInUser)) : ?>
                <li class="nav-item">
                    &nbsp &nbsp &nbsp
                </li>
                <li class="nav-item navbar-text">
<!--                    <a class="nav-link" href="profile.php">-->
                        <b>Hello, <?= $loggedInUser->name ?></b>
<!--                    </a>-->
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">Logout</a>
                </li>
            <?php else: ?>
            <li class="nav-item">
                <a class="nav-link" href="register.php">Register</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="login.php">Login</a>
            </li>
            <?php endif; ?>
<!--            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Dropdown
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="home.php">Home</a>
                    <a class="dropdown-item" href="product-search.php">Search</a>
                    <div class="dropdown-divider"></div>
                    <?php /*if (!empty($loggedInUser)) : */?>
                        &nbsp &nbsp &nbsp Hello, <?/*= $loggedInUser->name */?>
                        <a class="dropdown-item" href="logout.php">Logout</a>
                    <?php /*else: */?>
                        <a class="dropdown-item" href="register.php">Register</a>
                        <a class="dropdown-item" href="login.php">Login</a>
                    <?php /*endif; */?>
                </div>
            </li>-->
        </ul>
    </div>
</nav>

