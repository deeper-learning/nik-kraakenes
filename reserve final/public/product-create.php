<?php

use App\Entity\Product;

require_once '../src/setup.php';

if (!empty($_POST['title']) && !empty($_POST['description']) && !empty($_POST['category']) && !empty($_POST['crew']) && !empty($_POST['weapons']) && !empty($_POST['endurance'])) {
    $formData = [
        'title' => strip_tags($_POST['title']),
        'description' => strip_tags($_POST['description']),
        'category' => strip_tags($_POST['category']),
        'crew' => strip_tags($_POST['crew']),
        'weapons' => strip_tags($_POST['weapons']),
        'endurance' => strip_tags($_POST['endurance']),
    ];

    $formProduct = new Product();
    $formProduct->title = $formData['title'];
    $formProduct->description = $formData['description'];
    $formProduct->category = $formData['category'];
    $formProduct->crew = $formData['crew'];
    $formProduct->weapons = $formData['weapons'];
    $formProduct->endurance = $formData['endurance'];

    // Create Product
    $product = $dbProvider->createProduct($formProduct);
    $logger->info('Product created: ' . $product->title);
    header('Location: product.php?productId=' . $product->id);
    exit;
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'template_parts/header_includes.php' ?>
    <!--
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <link href="engine2.css" rel="stylesheet">
    -->
    <title>Create Product</title>

</head>
<body>
<div class="container">
<?php /*include 'template_parts/navbar.php'; */?>
    <div class="card p-4 stickery">
        <h1>Create Product</h1>
        <form method="post">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <label class="whitely" for="title">Title</label>
                    <input class="form-control" name="title" id="title" placeholder="Title">
                    <label class="whitely" for="description">Description</label>
                    <textarea class="form-control" name="description" id="description" placeholder="Description" rows="10"></textarea>
                </div>
                <div class="col-md-6 col-sm-6">
                    <label class="whitely" for="category">Categories</label>
                    <input class="form-control" name="category" id="category" placeholder="Categories">
                    <label class="whitely" for="crew">Crew & Passengers</label>
                    <input class="form-control" name="crew" id="crew" placeholder="Crew">
                    <label class="whitely" for="weapons">Weaponry</label>
                    <input class="form-control" name="weapons" id="weapons" placeholder="Weapons">
                    <label class="whitely" for="endurance">Effective Range</label>
                    <input class="form-control" name="endurance" id="endurance" placeholder="Range">
                </div>
            </div>
            <br>
            <button type="submit" class="btn btn-secondary">Create</button>
        </form>
    </div>
</div>

<?php include 'template_parts/footer_includes.php'; ?>
</body>
</html>
