<?php

require_once '../src/setup.php';

// product not found test
if (!isset($_GET['productId'])) {
    die("Missing productId in URL");
}

$productId = $_GET['productId'];

$product = $dbProvider->getProduct($productId);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'template_parts/header_includes.php' ?>

    <title>Rating Rockets</title>

</head>
<body>

<div class="container">
    <?php include 'template_parts/navbar_includes.php' ?>
    <br>
    <div class="card p-4 stickery">
        <div class="row">
            <div class="col-md-6 col-sm-6">
            </div>
            <div class="col-md-6 col-sm-6">
                <h1 class="card-title"><b><?= $product->title ?></b></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <img class="active img-fluid d-block w-100 imgary" src="<?= $product->imagePath ?>"  alt="<?= $product->title ?>">
                <br>
                <button type="button" class="btn btn-outline-warning" data-toggle="modal" data-target="#checkinModal">
                    Rate this Rocket
                </button>
                <br> &nbsp
            </div>
            <div class="col-md-6 col-sm-6">
                <p class="card-text fs-6"><?= $product->description ?></p>
            </div>
        </div>
    </div>

    <h2 class="my-4">Additional Information</h2>

    <div class="card p-4 my-4 stickery">
        <table class="table stickery">
            <tbody>
            <tr>
                <th class="whitely">Average Rating</th>
                <td class="whitely">
                    <div class="star-rating"><div style="width:<?= $product->average_rating * 20; ?>%;"></div></div>
                </td>
            </tr>
            <tr>
                <th class="whitely">Crew & Passengers</th>
                <td class="whitely"><p><?= $product->crew ?></p></td>
            </tr>
            <tr>
                <th class="whitely">Armaments</th>
                <td class="whitely"><p><?= $product->weapons ?></p></td>
            </tr>
            <tr>
                <th class="whitely">Effective Range</th>
                <td class="whitely"><p><?= $product->endurance ?></p></td>
            </tr>
            </tbody>
        </table>
    </div>

    <h2 class="my-4">Recent Reviews</h2>

    <div id="checkins">
        <?php foreach ($product->getCheckins() as $checkIn): ?>
            <div class="card p-4 my-4 stickery">
                <h3>
                    <div class="col-md-6 col-sm-6 d-inline-block"><?= $checkIn->name ?></div>
                    <div class="star-rating"><div style="width:<?= $checkIn->rating * 20; ?>%;"></div>
                </h3>
                <p><?= $checkIn->review ?></p>
            </div>
        <?php endforeach; ?>
    </div>

    <div class="modal fade" id="checkinModal" tabindex="-1" aria-labelledby="checkinModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="checkin-create.php">
                    <div class="modal-header stickery">
                        <h5 class="modal-title" id="checkinModalLabel">Rate this Rocket</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body stickery">
                        <input type="hidden" name="product_id" value="<?= $_GET['productId'] ?>">
                        <div class="form-group">
                            <label for="name">Your Name</label>
                            <input type="text" name="name" id="name" placeholder="Enter name" aria-describedby="yourName" value="<?= $loggedInUser->name ?? '' ?>">
                            <small id="yourName" class="form-text text-warning">Please enter your name.</small>
                        </div>
                        <div class="form-group">
                            <label for="rating">Your Rating</label>
                            <input type="number" class="form-control" name="rating" id="rating" aria-describedby="yourRating" placeholder="Enter rating" min="1" max="5">
                            <small id="yourRating" class="form-text text-warning">Rate the product from 1 to 5.</small>
                        </div>
                        <div class="form-group">
                            <label for="review">Review</label>
                            <textarea class="form-control" id="review" name="review" aria-describedby="yourReview" placeholder="Additional comments"></textarea>
                            <small id="yourReview" class="form-text text-warning">What did you think?</small>
                        </div>
                    </div>
                    <div class="modal-footer stickery">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-outline-warning">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>

<!-- Optional JavaScript -->
<?php include 'template_parts/footer_includes.php'; ?>

<!-- Axios-->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

</body>
</html>
