<?php

require_once '../src/setup.php';


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include 'template_parts/header_includes.php' ?>

    <title>Rating Rockets</title>

</head>
<body>
<div class="container">
    <?php include 'template_parts/navbar_includes.php' ?>
    <br>
    <div class="col-sm-12">
        <div class="row">
            <h1>Welcome to <b>Rating Rockets</b></h1>
            <br>
        </div>
        <div class="col-sm-12 d-inline-flex">
<!--            <div class="row">-->
                <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="carousel-item active" data-interval="10000">
                            <a href="product.php?productId=10">
                                <img class="active img-fluid d-block w-100 imgary" src="assets/saturn.jpg"  alt="The Saturn V">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=5">
                                <img class="img-fluid d-block w-100 imgary" src="assets/thunder.jpg" alt="UED Thunderfighter">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=2">
                                <img class="img-fluid d-block w-100 imgary" src="assets/ajax.jpg"  alt="War Rocket Ajax">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=3">
                                <img class="img-fluid d-block w-100 imgary" src="assets/eagle.jpg"  alt="Eagle Transporter">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=9">
                                <img class="img-fluid d-block w-100 imgary" src="assets/cr90corvette.jpg"  alt="Blockade Runner">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=8">
                                <img class="active img-fluid d-block w-100 imgary" src="assets/normandy.jpg"  alt="The Normandy SR-2">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=11">
                                <img class="img-fluid d-block w-100 imgary" src="assets/liberator.jpg" alt="The Liberator">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=15">
                                <img class="img-fluid d-block w-100 imgary" src="assets/shuttle.jpg"  alt="Space Shuttle">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=12">
                                <img class="img-fluid d-block w-100 imgary" src="assets/y-wing.jpg"  alt="Y-Wing Fighter">
                            </a>
                        </div>
                        <div class="carousel-item" data-interval="10000">
                            <a href="product.php?productId=16">
                                <img class="img-fluid d-block w-100 imgary" src="assets/battlestar.jpg"  alt="Battlestar Galactica">
                            </a>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
<!--            </div>-->
        </div>

        <div class="col-md-12 col-sm-12">
            <p class="card-text fs-6">Here at <b>Rating Rockets</b> we present to you a fine selection of spacecraft,
                both real, proposed, and fictional. Join our site to add your own comments and ratings for these
                majestic and iconic craft. Search our database to find specific characteristics or qualities,
                and never lose track of these classics ever again.</p>
        </div>

        </div>

    </div>

</div>

<!-- Optional JavaScript -->
<?php include 'template_parts/footer_includes.php'; ?>

<!-- Axios-->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

</body>
</html>
