<?php

use App\DataProvider\DatabaseProvider;

require_once '../vendor/autoload.php';

$whoops = new \Whoops\Run();
$whoops->pushHandler(
    new \Whoops\Handler\PrettyPageHandler()
);
$whoops->register();

$dotenv = \Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$logger = new \Monolog\Logger('application');
/*$logger->pushHandler(
    new \Monolog\Handler\StreamHandler(
        'application.log' ,
        \Monolog\Logger::WARNING
    )
);*/

// new logger
$logger->pushHandler(
    new \Monolog\Handler\RotatingFileHandler(
        dirname(__DIR__) . DIRECTORY_SEPARATOR . 'logs' . DIRECTORY_SEPARATOR . 'application.log',
         5,
         \Monolog\Logger::WARNING
    )
);

// dev env chrome plugin
if (isset($_ENV['APP_ENV']) && $_ENV['APP_ENV'] === 'development') {
    $logger->pushHandler(
        new \Monolog\Handler\ChromePHPHandler()
    );
}

$dbProvider = new DatabaseProvider();

session_start();

if (isset($_SESSION['loginId'])) {
   $loggedInUser = $dbProvider->getUser($_SESSION['loginId']);
}


/*try {
    $dbh = new PDO(
        'mysql:host=mysql;dbname=project',
        $_ENV['DBUSERNAME'],
        $_ENV['DBPASSWORD']
    );
} catch (PDOException $e) {
    // We could log this!
    die('Unable to establish a database connection');
}*/


