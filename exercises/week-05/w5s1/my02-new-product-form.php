<?php

require_once 'db.php';

$userAddedSuccess = false;
$reviewAddedSuccess = false;
$ratingAddedSuccess = false;

if (!empty($_POST)) {
    $newUser = $_POST['user'];

    $stmt = $dbh->prepare('INSERT INTO checkins (user_name, submitted) VALUES (:user)');

    $stmt->execute([
        'user' => $newUser,
    ]);

    echo '# Rows affected: ' . $stmt->rowCount();

    $userAddedSuccess = true;
}

if (!empty($_POST)) {
    $newReview = $_POST['review'];

    $stmt = $dbh->prepare('INSERT INTO checkins (review) VALUES (:review)');

    $stmt->execute([
        'review' => $newReview,
    ]);

    $reviewAddedSuccess = true;
}

if (!empty($_POST)) {
    $newReview = $_POST['rating'];

    $stmt = $dbh->prepare('INSERT INTO checkins (rating) VALUES (:rating)');

    $stmt->execute([
        'rating' => $newReview,
    ]);

    $ratingAddedSuccess = true;
}

?>

<!doctype html>
<html>
<head>
    <title>Full Page</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
          crossorigin="anonymous"
    >
</head>
<body class="container pt-4">
<!--    <form action="" method="post">

        <?php /*if ($productAddedSuccess): */?>
        <p class="alert alert-success">Product added successfully!</p>
        <?php /*endif; */?>

        <div class="form-group">
            <label for="product-title">Title</label>
            <input type="text" name="title" id="product-title" class="form-control">
        </div>

        <button type="submit" class="btn btn-success">Save Product</button>
    </form>-->

    <div class="container">
        <form action="" method="post">

            <?php if ($userAddedSuccess): ?>
                <p class="alert alert-success">User added successfully!</p>
            <?php endif; ?>

            <?php if ($reviewAddedSuccess): ?>
                <p class="alert alert-success">Review added successfully!</p>
            <?php endif; ?>

            <?php if ($ratingAddedSuccess): ?>
                <p class="alert alert-success">Rating added successfully!</p>
            <?php endif; ?>

            </div>
            <div class="form-group">
                <label for="inputUser">User Name</label>
                <input type="text" name="user" class="form-control" id="inputUser" placeholder="Joe Doe">
            </div>
            <div class="form-group">
                <label for="reviewContent">Review</label>
                <textarea class="form-control"  name="review" id="reviewContent" rows="3"></textarea>
            </div>
            <div class="form-row">
                <div class="form-group col-1">
                    <label for="reviewRating">Rating</label>
                    <select class="form-control" name="rating" id="reviewRating">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="gridCheck">
                    <label class="form-check-label" for="gridCheck">
                        Check me out
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Sign in</button>
        </form>
    </div>

</body>
</html>
