-- Add a simple product table
CREATE TABLE `checkins` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`user_name` VARCHAR(31) NOT NULL,
	`rating` INT(5) NOT NULL,
	`review` VARCHAR(255) NOT NULL,
	`submitted` TIMESTAMP NOT NULL,
	PRIMARY KEY (`id`)
);
