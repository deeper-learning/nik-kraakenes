-- UPDATE table_name SET columnA = 'new value';


-- UPDATE product SET title = 'Protractor';


UPDATE product SET title = 'Protractor' WHERE title = 'Ruler' AND id = 3;

SELECT id, title FROM  product
-- Error Code: 1175. You are using safe update mode and you tried to update a table without a WHERE that uses a KEY column.  To disable safe mode, toggle the option in Preferences -> SQL Editor and reconnect.
