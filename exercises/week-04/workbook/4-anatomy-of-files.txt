
name: The original filename from the uploader's device
type: The MIME type of the file uploaded
tmp_name: Where the uploaded file is temporarily stored, ready to be moved
error: An error code. Any non-0 value indicates an error
size: The size of the uploaded file in bytes

[
  'name' => 'cat.jpg',
  'type' => 'image/jpeg',
  'tmp_name' => '/private/var/folders/fr/89d4rzhx1r9_5q23wld5dg080000gp/T/phpbqIzJE',
  'error' => 0,
  'size' => 40144
]
