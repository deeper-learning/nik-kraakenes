<?php

$result = '';

if (!empty($_POST)) {
    $result = $_POST['a'] + $_POST['b'];
    echo $result;
    die();
}

if (!empty($_GET)) {
    $result = $_GET['a'] + $_GET['b'];
        echo $result;
        die();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS --//TODO: get 4.6 instead-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

    <link href="engine.css" rel="stylesheet">

    <title>Mattys</title>

</head>
<body>

<div class="container p-4 my-4 col-md-8">
    <h1>Math Wimble</h1>
    <form action="" method="post" class="form p-4 m-4" id="form">
    <div class="form-inline">
        <div class="form-group col-md-4 d-inline-block">
            <label for="a">A</label>
            <input type="number" name="a" id="a" class="form-control">
        </div>
        <div class="form-group p-4 col-md-6">
            <label for="operaTor">Operation</label>
            <select class="form-control" id="operaTor">
                <option>+</option>
                <option>-</option>
                <option>x</option>
                <option>÷</option>
            </select>
        </div>
        <div class="form-group col-md-4 d-inline-block">
            <label for="b">B</label>
            <input type="number" name="b" id="b" class="form-control">
        </div>
        <div class="form-group col-md-4">
            <label for="result">Result:</label>
            <input type="text" name="result" id="result" class=form-control" readonly>
        </div>
        <div class="form-group">
            <button type="submit" class="btn-bd-primary">Calculate</button>
        </div>
    </div>
    </form>
</div>

</body>

<script>
    window.addEventListener('DOMContentLoaded' , function() {
        let form = document.getElementById('form');
        form.addEventListener('submit' , function (e) {
            e.preventDefault();
            calculate();
        });
    });
    function calculate() {
        let a = document.getElementById('a')
        let b = document.getElementById('b')
        let result = document.getElementById('result')

        axios.get('mathybits.php?a=' + a.value +'&b=' + b.value)
        .then(function (response) {
            console.log(response.data);
            result.value = response.data;
        })
        .catch(function (error)) {
            console.log(error);
        })
    }
</script>


<!-- Optional JavaScript -->
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

<!-- Axios-->
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>


</html>
