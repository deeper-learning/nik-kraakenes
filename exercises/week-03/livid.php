<?php //declare(strict_types=1);

class User
{
    public string $name;
    public int $age;
    public array $pocketChange

    public function __construct($name , $age)
    {
        $this->name = $name;
        $this->age = $age;
    }

}

$user = new User ('Owen', 'false');

echo $user->age();

?>